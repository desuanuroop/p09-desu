//
//  Level.swift
//  SweetCrush
//
//  Created by Anuroop Desu on 5/3/16.
//  Copyright © 2016 adesu1. All rights reserved.
//

import Foundation

let NumColumns = 9
let NumRows = 9

class Level {
    private var cookies = Array2D<Cookie>(columns: NumColumns, rows: NumRows)
    private var tiles = Array2D<Tile>(columns: NumColumns, rows: NumRows)
    var targetScore: Int = 0
    var moves: Int = 0
    var targetMoves: Int = 0
    var score = 0
    init(filename: String) {
        if let dictionary = Dictionary<String, AnyObject>.loadJSONFromBundle(filename) {
            // 2
            if let tilesArray: AnyObject = dictionary["tiles"] {
                // 3
                for (row, rowArray) in (tilesArray as! [[Int]]).enumerate() {
                    // 4
                    let tileRow = NumRows - row - 1
                    // 5
                    for (column, value) in rowArray.enumerate() {
                        if value == 1 {
                            tiles[column, tileRow] = Tile()
                        }
                    }
                }
            }
            targetScore = (dictionary["targetScore"]! as? Int)!
            moves = (dictionary["moves"]! as? Int)!
            targetMoves = moves
            print (targetScore, moves)
            
        }
    }
    
    func cookieAtColumn(column: Int, row: Int) -> Cookie? {
        assert(column >= 0 && column < NumColumns)
        assert(row >= 0 && row < NumRows)
        return cookies[column, row]
    }
    
    func shuffle() -> Set<Cookie> {
        NSLog("In Shuffle")
        return createInitialCookies()
    }
    
    private func createInitialCookies() -> Set<Cookie> {
        var set = Set<Cookie>()
        for row in 0..<NumRows {
            for column in 0..<NumColumns {
                if (tiles[column, row] != nil) {
                    var cookieType: CookieType
                    repeat {
                        cookieType = CookieType.random()
                    }
                        while (column >= 2 &&
                            cookies[column - 1, row]?.cookieType == cookieType &&
                            cookies[column - 2, row]?.cookieType == cookieType)
                            || (row >= 2 &&
                                cookies[column, row - 1]?.cookieType == cookieType &&
                                cookies[column, row - 2]?.cookieType == cookieType)

                    let cookie = Cookie(column: column, row: row, cookieType: cookieType)
                    cookies[column, row] = cookie
                    set.insert(cookie)
                }
            }
        }
        return set
    }
    
    func tileAtColumn(column: Int, row: Int) -> Tile? {
        assert(column >= 0 && column < NumColumns)
        assert(row >= 0 && row < NumRows)
        return tiles[column, row]
    }
    
    func checkSwap(source: Cookie)->Bool {
        let retL = lookLeft(source)
        let retU = lookUP(source)
        let retD = lookDown(source)
        let retR = lookRight(source)
        if ((retL+retR > 1) || (retU+retD > 1)) {
            return true
        }else {
            return false
        }
    }
    func lookLeft(source: Cookie)-> (Int) {
        var count: Int = 0
        for i in (source.column+1..<NumColumns) {
            if(source.cookieType == cookies[i, source.row]?.cookieType) {
                count += 1
            } else {
                break
            }
        }// END of for
        return count
    }
    
    func lookRight(source: Cookie)-> (Int) {
        var count: Int = 0
        for i in (0..<source.column).reverse() {
            if(source.cookieType == cookies[i, source.row]?.cookieType) {
                count += 1
            } else {
                break
            }
        }// END of for
        return count
    }
    func lookUP(source: Cookie) -> Int {
        var count: Int = 0
        for i in (source.row+1..<NumRows){
            if (source.cookieType == cookies[source.column, i]?.cookieType) {
                count += 1
            }else {
                break
            }
        }//End of for
        return count
    }

    func lookDown(source: Cookie)->Int {
        var count: Int = 0
        for i in (0 ..< source.row).reverse() {
            if (source.cookieType == cookies[source.column, i]?.cookieType) {
                count += 1
            }else {
                break
            }
        }//End of for
        return count
    }//End of func
        
    func performSwap(swap: Swap)->Bool {
        var columnA = swap.cookieA.column
        var rowA = swap.cookieA.row
        
        var columnB = swap.cookieB.column
        var rowB = swap.cookieB.row
        
        cookies[columnA, rowA] = swap.cookieB
        swap.cookieB.column = columnA
        swap.cookieB.row = rowA
        
        cookies[columnB, rowB] = swap.cookieA
        swap.cookieA.column = columnB
        swap.cookieA.row = rowB
        if ((checkSwap(cookies[columnB, rowB]!) || (checkSwap(cookies[columnA, rowA]!)))) {
            return true
        } else {
            //Re-trace Swap Back
            columnA = swap.cookieB.column
            rowA = swap.cookieB.row
            
            columnB = swap.cookieA.column
            rowB = swap.cookieA.row
            
            cookies[columnA, rowA] = swap.cookieA
            swap.cookieB.column = columnB
            swap.cookieB.row = rowB
            
            cookies[columnB, rowB] = swap.cookieB
            swap.cookieA.column = columnA
            swap.cookieA.row = rowA
            
        return false
        }
        //return true
    }
    
    //Finding chains among cookies
    func removeMatches() -> Set<Chain> {
        let horizontalChains = detectHorizontal()
        let verticalChains = detectVertical()
        
        removeCookies(horizontalChains)
        removeCookies(verticalChains)
        
        calculateScore(horizontalChains)
        calculateScore(verticalChains)
        
        return horizontalChains.union(verticalChains)
    }
    
    private func detectHorizontal() ->Set<Chain> {
        var set = Set<Chain>()
        
        for row in 0..<NumRows {
            for var col in 0..<NumColumns-2 {
                if let cookie = cookies[col, row] {
                    let matchType = cookie.cookieType
                    
                    if cookies[col+1, row]?.cookieType == matchType &&
                        cookies[col+2, row]?.cookieType == matchType {
                            let chain = Chain(chainType: .Horizontal)
                            
                            repeat {
                                chain.addCookie(cookies[col, row]!)
                                ++col
                            } while col < NumColumns && cookies[col, row]?.cookieType == matchType
                            
                            set.insert(chain)
                            continue
                    }
                }
                ++col
            }
        }
        return set
    }
    
    private  func detectVertical() -> Set<Chain> {
        var set = Set<Chain>()
        
        for col in 0..<NumColumns {
            for var row in 0..<NumRows-2 {
                if let cookie = cookies[col, row] {
                    let matchType = cookie.cookieType
                    
                    if cookies[col, row+1]?.cookieType == matchType &&
                        cookies[col, row+2]?.cookieType == matchType {
                            let chain = Chain(chainType: .Horizontal)
                            
                            repeat {
                                chain.addCookie(cookies[col, row]!)
                                ++row
                            } while row < NumColumns && cookies[col, row]?.cookieType == matchType
                            
                            set.insert(chain)
                            continue
                    }
                }
                ++row
            }
        }
        return set
    }
    
    //Cookies Removal
    private func removeCookies(chains: Set<Chain>) {
        for chain in chains {
            for cookie in chain.cookies {
                cookies[cookie.column, cookie.row] = nil
            }
        }
    }
    
    //Fill holes by moving cookies
    func fillHoles() -> [[Cookie]] {
        var columns = [[Cookie]]()
        
        for column in 0..<NumColumns {
            var array = [Cookie]()
            for row in 0..<NumRows {
                if tiles[column, row] != nil && cookies[column, row] == nil {
                    for lookup in (row+1)..<NumRows {
                        if let cookie = cookies[column, lookup] {
                            cookies[column, lookup] = nil
                            cookies[column, row] = cookie
                            cookie.row = row
                            
                            array.append(cookie)
                            break
                        }
                    }
                }
            }
            if !array.isEmpty {
                columns.append(array)
            }
        }
        return columns
    }
    
    //Adding New Cookies to empty tiles
    func topUp()->[[Cookie]] {
        var columns = [[Cookie]]()
        var cookieType: CookieType = .Unknown
        
        for column in 0..<NumColumns {
            var array = [Cookie]()
            
            for var row=NumRows-1; row>=0 && cookies[column, row] == nil; --row {
                if tiles[column, row] != nil {
                    var newCookieType: CookieType
                    
                    repeat {
                        newCookieType = CookieType.random()
                    }while newCookieType == cookieType
                    cookieType = newCookieType
                    
                    let cookie = Cookie(column: column, row: row, cookieType: cookieType)
                    cookies[column, row] = cookie
                    array.append(cookie)
                }
            }
            if !array.isEmpty {
                columns.append(array)
            }
        }
        return columns
    }
    
    func calculateScore(chains: Set<Chain>) {
        for chain in chains {
            chain.score = 60 * (chain.length - 2)
        }
    }
}