//
//  GameScene.swift
//  SweetCrush
//
//  Created by Anuroop Desu on 5/3/16.
//  Copyright (c) 2016 adesu1. All rights reserved.
//

import SpriteKit
class GameScene: SKScene {
    var background = SKSpriteNode(imageNamed: "Background")
    var bg = SKSpriteNode(imageNamed: "Background")
    var playLabel = SKLabelNode()
    
    var level = Level!()
    
    let TileWidth: CGFloat = 45.0
    let TileHeight: CGFloat = 55.0
    
    let CookiesWidth: CGFloat = 40.0
    let CookiesHeight: CGFloat = 50.0
    
    let gameLayer = SKNode()
    let cookiesLayer = SKNode()
    let tilesLayer = SKNode()
    
    let NumColumns = 9
    let NumRows = 9
    
    var swipeFromColumn: Int?
    var swipeFromRow: Int?
    
    var flag: Bool = false
    //For swipe
    var swipeHandler: ((Swap) -> ())?
    var selectionSprite = SKSpriteNode()
    
    //Swipe Sounds
    let swapSound = SKAction.playSoundFileNamed("/Sounds/Chomp.wav", waitForCompletion: false)
    let invalidSwapSound = SKAction.playSoundFileNamed("/Sounds/Error.wav", waitForCompletion: false)
    let matchSound = SKAction.playSoundFileNamed("/Sounds/Ka-Ching.wav", waitForCompletion: false)
    let fallingCookieSound = SKAction.playSoundFileNamed("/Sounds/Scrape.wav", waitForCompletion: false)
    let addCookieSound = SKAction.playSoundFileNamed("/Sounds/Drip.wav", waitForCompletion: false)
    
    //SKLabelNodes
    var targetLabel = SKLabelNode()
    var targetScoreLabel = SKLabelNode()
    var movesLabel = SKLabelNode()
    var movesScoreLabel =  SKLabelNode()
    var totalScoreLabel = SKLabelNode()
    var scoreLabel = SKLabelNode()
    var score: Int = 0
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        swipeFromColumn = nil
        swipeFromRow=nil
        //Set Background Image
        background.position = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        background.size = frame.size
        background.zPosition = -5
        addChild(background)
        
        bg.position = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        bg.size = frame.size
        bg.zPosition = 100
        addChild(bg)
        
        //playLabel
        playLabel.text = "Touch To Play"
        playLabel.zPosition = 101
        playLabel.position = CGPointMake(frame.size.width/2, frame.size.height/2-50)
        playLabel.fontColor = UIColor .brownColor()
        playLabel.fontSize = 50
        playLabel.fontName = "Chalkduster"
        addChild(playLabel)
        
        
        //Add the Layers to game Scene
        addChild(gameLayer)
        
        let layerPosition = CGPoint(x: -TileWidth * CGFloat(NumColumns) / 2, y: -TileHeight * CGFloat(NumRows) / 2)
        cookiesLayer.position = layerPosition
        tilesLayer.position = layerPosition
        gameLayer.addChild(cookiesLayer)
        gameLayer.addChild(tilesLayer)
        
        //Adding Cookies to Screen.
        level = Level(filename: "/Levels/Level_1")
        addTiles()
        beginGame()
        
        //Swipes
        swipeHandler = handleSwipe
        
        //Add data to labels
        targetLabel.text = "Target"
        targetScoreLabel.text = String(level.targetScore)
        movesLabel.text = "Moves"
        movesScoreLabel.text = String(level.moves)
        totalScoreLabel.text = "Score"
        scoreLabel.text = "0"
        score = 0
        
        //place labels
        targetScoreLabel.position = CGPointMake(380, 690)
        movesScoreLabel.position = CGPointMake(520, 690)
        totalScoreLabel.position = CGPointMake(660,730)
        
        targetLabel.position = CGPointMake(380, 730)
        movesLabel.position = CGPointMake(520,730)
        scoreLabel.position = CGPointMake(660, 690)
        //Font of Labels
        targetLabel.fontName = "Gill Sans Bold"
        targetLabel.fontSize = 24.0
        movesLabel.fontName = "Gill Sans Bold"
        movesLabel.fontSize = 24.0
        totalScoreLabel.fontName = "Gill Sans Bold"
        totalScoreLabel.fontSize = 24.0
        
        targetScoreLabel.fontName = "Gill Sans Bold"
        targetScoreLabel.fontSize = 34.0
        movesScoreLabel.fontName = "Gill Sans Bold"
        movesScoreLabel.fontSize = 34.0
        scoreLabel.fontName = "Gill Sans Bold"
        scoreLabel.fontSize = 34.0
        //Add labels
        addChild(targetLabel)
        addChild(targetScoreLabel)
        addChild(movesLabel)
        addChild(movesScoreLabel)
        addChild(totalScoreLabel)
        addChild(scoreLabel)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        if (bg.zPosition == 100 ) {
            bg.zPosition = -10
            bg.runAction(SKAction.sequence([
                SKAction.fadeOutWithDuration(1.0)]))
            playLabel.hidden = true
            level.moves++
            return
        }
        let touch = touches.first
        let location = touch!.locationInNode(cookiesLayer)
        let (success, column, row) = convertPoint(location)
        if success {
            if let cookie = level.cookieAtColumn(column, row: row) {
                swipeFromColumn = column
                swipeFromRow = row
                showHighlighted(cookie)
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if swipeFromColumn == nil {return}
        let touch = touches.first
        let location = touch!.locationInNode(cookiesLayer)
        let (success, column, row) = convertPoint(location)
        //Check for direction of movement.
        if success {
            var horzDelta = 0, vertDelta = 0
            if column < swipeFromColumn! {
                horzDelta = -1
            } else if column > swipeFromColumn! {
                horzDelta = 1
            } else if row < swipeFromRow! {
                vertDelta = -1
            } else if row > swipeFromRow! {
                vertDelta = 1
            }
            
            if horzDelta != 0 || vertDelta != 0 {
                trySwapHorizontal(horzDelta, vertical: vertDelta)
                swipeFromColumn = nil
            }
        }
        removeHighlighted()
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        level.moves--
        if selectionSprite.parent != nil && swipeFromColumn != nil {
            removeHighlighted()
        }
        swipeFromColumn = nil
        swipeFromRow = nil
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        touchesEnded(touches!, withEvent: event)
    }

    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        movesScoreLabel.text = String(level.moves)
        scoreLabel.text = String(score)
        if (score >= level.targetScore && !flag) {
            //Won the game.
            flag = true
            print ("Uou won the game")
            let alert = UIAlertView()
            alert.delegate = self
            alert.title = "Game Over"
            alert.message = "You Won !!"
            alert.addButtonWithTitle("Play Again")
            alert.addButtonWithTitle("Exit")
            alert.show()
        }
        else if (level.moves == 0 && score < level.targetScore && !flag) {
            //lost the game.
            flag = true
            let alert = UIAlertView()
            alert.delegate = self
            alert.title = "Game Over"
            alert.message = "You Lost !!"
            alert.addButtonWithTitle("Play Again")
            alert.addButtonWithTitle("Exit")
            alert.show()
        }
    }
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        
        switch buttonIndex{
            
        case 1:
            NSLog("exit");
            exit(1)
            break;
        case 0:
            //highScoreFlag = false // Replay
            score = 0
            movesScoreLabel.text = String(level.targetMoves)
            level.moves = level.targetMoves
            flag = false
            break;
        default:
            NSLog("Default");
            break;
            //Some code here..
            
        }
    }
    //Add Cookies to game Layer.
    func addSpritesForCookies(cookies: Set<Cookie>) {
        for cookie in cookies {
            let sprite = SKSpriteNode(imageNamed: cookie.cookieType.spriteName)
            sprite.position = pointForColumn(cookie.column, row:cookie.row)
            cookiesLayer.addChild(sprite)
            cookie.sprite = sprite
        }
    }
    //Add tiles has cookie backgrounds.
    func addTiles() {
        for row in 0..<NumRows {
            for column in 0..<NumColumns {
                if let _ = level.tileAtColumn(column, row: row) {
                    let tileNode = SKSpriteNode(imageNamed: "Tile")
                    tileNode.position = pointForColumn(column, row: row)
                    tileNode.zPosition = -1
                    tilesLayer.addChild(tileNode)
                }
            }
        }
    }
    
    func pointForColumn(column: Int, row: Int) -> CGPoint {
        return CGPoint(
            x: CGFloat(column)*TileWidth + TileWidth/2 + frame.size.width/2,
            y: CGFloat(row)*TileHeight + TileHeight/2 + frame.size.height/2)
    }
   
    func convertPoint(point: CGPoint) -> (success: Bool, column: Int, row: Int) {
        if point.x >= 0 && point.x < (CGFloat(NumColumns)*TileWidth + frame.size.width/2) &&
            point.y >= 0 && point.y < (CGFloat(NumRows)*TileHeight  + frame.size.height/2) {
                return (true, Int( (point.x - (frame.size.width/2)) / TileWidth), Int( (point.y - (frame.size.height/2)) / TileHeight))
        } else {
            return (false, 0, 0)  // invalid location
        }
    }
    
    func beginGame() {
        NSLog("In Begin game")
        shuffle()
    }
    
    func shuffle() {
        let newCookies = level.shuffle()
        addSpritesForCookies(newCookies)
    }
    
    //Function which handle swaps among cookies
    func trySwapHorizontal(horzDelta: Int, vertical vertDelta: Int) {
        let toColumn = swipeFromColumn! + horzDelta
        let toRow = swipeFromRow! + vertDelta
        
        if toColumn < 0 || toColumn >= NumColumns {return}
        if toRow < 0 || toRow >= NumRows {return}
        
        if let toCookie = level.cookieAtColumn(toColumn, row: toRow) {
            if let fromCookie = level.cookieAtColumn(swipeFromColumn!, row: swipeFromRow!) {
                if let handler = swipeHandler {
                    let swap = Swap(cookieA: fromCookie, cookieB: toCookie)
                    handler(swap)
                }
            }
        }
    }
    //Functions to animate valid and invalid swaps.
    func animateSwap(swap: Swap, completion: () -> ()) {
        let spriteA = swap.cookieA.sprite!
        let spriteB = swap.cookieB.sprite!
        
        spriteA.zPosition = 100
        spriteB.zPosition = 90
        
        let Duration: NSTimeInterval = 0.3
        
        runAction(swapSound)
        let moveA = SKAction.moveTo(spriteB.position, duration: Duration)
        moveA.timingMode = .EaseOut
        spriteA.runAction(moveA, completion: completion)
        
        let moveB = SKAction.moveTo(spriteA.position, duration: Duration)
        moveB.timingMode = .EaseOut
        spriteB.runAction(moveB)
    }
    
    func animateInvalidSwap(swap: Swap, completion: ()-> ()) {
        let spriteA = swap.cookieA.sprite!
        let spriteB = swap.cookieB.sprite!
        
        spriteA.zPosition = 100
        spriteB.zPosition = 90
        
        let Duration: NSTimeInterval = 0.3
        
        runAction(invalidSwapSound)
        let moveA = SKAction.moveTo(spriteB.position, duration: Duration)
        moveA.timingMode = .EaseOut
        
        let moveB = SKAction.moveTo(spriteA.position, duration: Duration)
        moveB.timingMode = .EaseOut
        
        spriteA.runAction(SKAction.sequence([moveA, moveB]), completion: completion)
        spriteB.runAction(SKAction.sequence([moveB, moveA]))
    }
    
    func handleSwipe(swap: Swap){
        self.userInteractionEnabled = false
        if (level.performSwap(swap)) {
            animateSwap(swap) {
                self.userInteractionEnabled = true
                self.handleMatches()
            }
        }else {
            animateInvalidSwap(swap) {
            self.userInteractionEnabled = true
            }
        }
    }
    
    func showHighlighted(cookie: Cookie) {
        if selectionSprite.parent != nil {
            selectionSprite.removeFromParent()
        }
        
        if let sprite = cookie.sprite {
            let texture = SKTexture(imageNamed: cookie.cookieType.highlightedSpriteName)
            selectionSprite.size = texture.size()
            selectionSprite.runAction(SKAction.setTexture(texture))
            
            sprite.addChild(selectionSprite)
            selectionSprite.alpha = 1.0
        }
    }
    
    func removeHighlighted() {
        selectionSprite.runAction(SKAction.sequence([
            SKAction.fadeOutWithDuration(0.6),
            SKAction.removeFromParent()]))
    }
    
    //Handle Chains
    func handleMatches() {
        let chains = level.removeMatches()
        animateMatchedCookies(chains) {
            for chain in chains {
                self.score += chain.score
            }
            var columns = self.level.fillHoles()
            self.animateFallingCookies(columns) {
                self.view?.userInteractionEnabled = true
            }
            columns = self.level.topUp()
            self.animateNewCookies(columns) {
                self.view?.userInteractionEnabled = true
            }
        }
    }
    
    func animateMatchedCookies(chains: Set<Chain>, completion: () -> ()) {
        for chain in chains {
            animateScoreForChain(chain)
            for cookie in chain.cookies {
                if let sprite = cookie.sprite {
                    if sprite.actionForKey("removing") == nil {
                        let scaleAction = SKAction.scaleTo(0.1, duration: 0.3)
                        scaleAction.timingMode = .EaseOut
                        sprite.runAction(SKAction.sequence([scaleAction, SKAction.removeFromParent()]),
                            withKey:"removing")
                    }
                }
            }
        }
        runAction(matchSound)
        runAction(SKAction.waitForDuration(0.3), completion: completion)
    }
    
    func animateFallingCookies(columns: [[Cookie]], completion: ()->()) {
        var longestDuration: NSTimeInterval = 0
        for array in columns {
            for (idx, cookie) in array.enumerate() {
                let newPosition = pointForColumn(cookie.column, row: cookie.row)
                let delay = 0.05 + 0.15*NSTimeInterval(idx)
                let sprite = cookie.sprite!
                let duration = NSTimeInterval(((sprite.position.y - newPosition.y) / TileHeight) * 0.1)
                longestDuration = max(longestDuration, duration + delay)
                let moveAction = SKAction.moveTo(newPosition, duration: duration)
                moveAction.timingMode = .EaseOut
                sprite.runAction(
                    SKAction.sequence([
                        SKAction.waitForDuration(delay),
                        SKAction.group([moveAction, fallingCookieSound])]))
            }
        }
        runAction(SKAction.waitForDuration(longestDuration), completion: completion)
    }
    
    func animateNewCookies(columns: [[Cookie]], completion: () -> ()) {
        var longestDuration: NSTimeInterval = 0
        
        for array in columns {
            let startRow = array[0].row + 1
            
            for (idx, cookie) in array.enumerate() {
                let sprite = SKSpriteNode(imageNamed: cookie.cookieType.spriteName)
                sprite.position = pointForColumn(cookie.column, row: startRow)
                cookiesLayer.addChild(sprite)
                cookie.sprite = sprite
                let delay = 0.1 + 0.2 * NSTimeInterval(array.count - idx - 1)
                let duration = NSTimeInterval(startRow - cookie.row) * 0.1
                longestDuration = max(longestDuration, duration + delay)
                let newPosition = pointForColumn(cookie.column, row: cookie.row)
                let moveAction = SKAction.moveTo(newPosition, duration: duration)
                moveAction.timingMode = .EaseOut
                sprite.alpha = 0
                sprite.runAction(
                    SKAction.sequence([
                        SKAction.waitForDuration(delay),
                        SKAction.group([
                            SKAction.fadeInWithDuration(0.05),
                            moveAction,
                            addCookieSound])
                        ]))
            }
        }
        runAction(SKAction.waitForDuration(longestDuration), completion: completion)
    }
    
    func animateScoreForChain(chain: Chain) {
        //Where to place the Score Label
        let firstSprite = chain.firstCookie().sprite!
        let lastSprite = chain.lastCookie().sprite!
        let centerPosition = CGPoint(
            x: (firstSprite.position.x + lastSprite.position.x)/2,
            y: (firstSprite.position.y + lastSprite.position.y)/2 - 8)
        
        //Animate the score label
        let scoreLabel = SKLabelNode(fontNamed: "GillSans-BoldItalic")
        scoreLabel.fontSize = 30
        scoreLabel.text = String(format: "%ld", chain.score)
        scoreLabel.position = centerPosition
        scoreLabel.zPosition = 300
        cookiesLayer.addChild(scoreLabel)
        
        let moveAction = SKAction.moveBy(CGVector(dx: 0, dy: 3), duration: 0.7)
        moveAction.timingMode = .EaseOut
        scoreLabel.runAction(SKAction.sequence([moveAction, SKAction.removeFromParent()]))
    }
}
